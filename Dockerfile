# MapServer from git master
# ~

FROM ubuntu:16.04 
MAINTAINER Guillaume Sueur <guillaume.sueur@neogeo-online.net>

ENV HOME /root
ENV GDAL_VERSION 1.11.4
ENV GEOS_VERSION 3.5.0
ENV PROJ_VERSION 4.9.1
RUN apt-get update && \
	apt-get install -y apache2 \
		apt-utils \
		nano \
		htop \
		wget \
		git \
		unzip \
		tar \
		bzip2 \
		build-essential \
		cmake \
		libfreetype6-dev \
		zlib1g-dev \
		libjpeg-dev \
		libcurl4-gnutls-dev \
		libxml2-dev \
		libcairo2-dev \
		libgif-dev \
        libpq-dev \
		libtiff5-dev \
        swig

RUN apt-get install -y libapache2-mod-fcgid libfcgi-dev
# Install GEOS
RUN apt-get install -y libgeos-3.5.0 libgeos-c1v5 libgeos-dbg libgeos-dev libgeos-doc libgeos-ruby1.8 ruby-geos libgeos++-dev
# Install Proj4
RUN cd /root && \
	wget http://download.osgeo.org/proj/proj-$PROJ_VERSION.tar.gz && \
	tar -xzf proj-$PROJ_VERSION.tar.gz && \
	cd proj-$PROJ_VERSION/nad && \
	wget http://download.osgeo.org/proj/proj-datumgrid-1.5.zip && \
	unzip proj-datumgrid-1.5.zip && \
	cd .. && \
	./configure --prefix=/usr && \
	make && \
	make install && \
	make clean && \
	/sbin/ldconfig

# Install GDAL
RUN cd /root && \
	wget http://download.osgeo.org/gdal/$GDAL_VERSION/gdal-$GDAL_VERSION.tar.gz  && \
	tar -zxf gdal-$GDAL_VERSION.tar.gz && \
	cd gdal-$GDAL_VERSION && \
	./configure --with-ogr --prefix=/usr && \
	make && \
	make install && \
	make clean && \
    /sbin/ldconfig
RUN echo "/usr/local/lib" >> /etc/ld.so.conf

RUN apt-get install -y language-pack-en-base && \
	export LC_ALL=en_US.UTF-8 && \
	export LANG=en_US.UTF-8 && \
	apt-get install -y python-software-properties software-properties-common curl && \
	add-apt-repository ppa:ondrej/php && \
	apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8 && \
	apt-get update && \
	apt-get install -y \
	php7.1 \
	php7.1-cli \
	php7.1-dev \
	php7.1-opcache \
	php7.1-bcmath \
	php7.1-bz2 \
	php7.1-common \
	php7.1-curl \
	php7.1-dba \
	php7.1-enchant \
	php7.1-gd \
	php7.1-imap \
	php7.1-json \
	php7.1-ldap \
	php7.1-mbstring \
	php7.1-mcrypt \
	php7.1-pspell \
	php7.1-readline \
	php7.1-soap \
	php7.1-xml \
	php7.1-zip \
	php7.1-mysql \
	php-imagick

# Install MapServer
RUN cd /root && git clone https://github.com/bjoernboldt/mapserver.git \ 
    && mkdir /root/mapserver/build \
    && cd /root/mapserver/build \
    && cmake .. -DWITH_THREAD_SAFETY=1 -DWITH_PHP=1 -DWITH_WMS=1 -DWITH_WFS=1 -DWITH_WCS=1 \ 
	-DWITH_CLIENT_WMS=1 -DWITH_CLIENT_WFS=1 -DWITH_SOS=0 -DWITH_KML=1 -DWITH_GEOS=1 -DWITH_GDAL=1 \
	-DWITH_OGR=1 -DWITH_PROJ=1 -DWITH_CAIRO=1 -DWITH_POSTGIS=1 -DWITH_FRIBIDI=0 -DWITH_HARFBUZZ=0 \
	-DWITH_ICONV=1 -DWITH_RSVG=0 -DWITH_MYSQL=0 -DWITH_CURL=1 -DWITH_LIBXML2=1 -DWITH_GIF=1 \ 
	-DWITH_EXEMPI=0 -DWITH_XMLMAPFILE=0 -DWITH_FCGI=1 \
    && make \ 
    && make install \
    && cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi \
    && /sbin/ldconfig

RUN a2enmod rewrite


# Set Apache environment variables (can be changed on docker run with -e)
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_SERVERADMIN admin@localhost
ENV APACHE_SERVERNAME localhost
ENV APACHE_SERVERALIAS docker.localhost
ENV APACHE_DOCUMENTROOT /var/www


EXPOSE 80 
# Add FCGI configuration
ADD ./apache/default /etc/apache2/sites-enabled/
ADD ./www /var/www
ADD ./start.sh /start.sh
RUN mkdir /var/maps \
&& rm /etc/apache2/sites-enabled/000-default.conf \
&& mv /etc/apache2/sites-enabled/default /etc/apache2/sites-enabled/000-default.conf
ADD ./maps /var/maps
RUN chmod 0755 /start.sh
RUN mkdir /var/www/tmp/

ADD ./php/20-mapserver.ini /etc/php/7.1/apache2/conf.d/20-mapserver.ini

#VOLUME /var/maps
#VOLUME /var/www

RUN chown -R www-data:www-data /var/maps
RUN chown -R www-data:www-data /var/www

CMD ["bash", "start.sh"]

